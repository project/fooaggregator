<?php // -*-php-*-

/**
 * Implementation of hook_form()
 */
function fooaggregator_feed_form(&$node, &$param) {
  $type = node_get_types('type', $node);

  $form['title'] = array(
			 '#type' => 'textfield',
			 '#title' => check_plain($type->title_label),
			 '#required' => TRUE,
			 '#default_value' => $node->title,
			 '#weight' => -5
			 );

  $form['body_filter']['body'] = array(
				       '#type' => 'textarea',
				       '#title' => check_plain($type->body_label),
				       '#default_value' => $node->body,
				       '#required' => FALSE,
				       );

  $form['body_filter']['filter'] = filter_form($node->format);

  $form['url'] = array(
		       '#title' => t('URL'),
		       '#type' => 'textfield',
		       '#required' => TRUE,
		       '#default_value' => $node->url,
		       );

  $refresh = drupal_map_assoc(array(900, 1800, 3600, 7200, 10800, 21600, 32400, 43200, 64800, 86400, 172800, 259200, 604800, 1209600, 2419200), 'format_interval');
  $refresh[0] = t('Disable');
  $form['refresh'] = array(
			   '#title' => t('Update interval'),
			   '#type' => 'select',
			   '#options' => $refresh,
			   '#default_value' => $node->refresh ? $node->refresh : 7200,
			   );

  // todo: discuss a better alternative.
  $form['visible'] = array(
			   '#title' => t('Visible in the main aggregator page.'),
			   '#type' => 'checkbox',
			   '#default_value' => $node->visible,
			   '#description' => t('This setting determines the visibility of this feed in the <a href="@page_url">news aggregator</a>', array('@page_url' => url('fooaggregator'))),
			   );

  $form['image'] = array(
			 '#title' => t('Image'),
			 '#type' => 'fieldset',
			 '#collapsed' => false,
			 '#collapsible' => true,
			 );
  $form['image']['image_title'] = array(
					'#type' => 'textfield',
					'#title' => t('Title for the image.'),
					'#default_value' => $node->image_title,
					);
  $form['image']['image_link'] = array(
				       '#type' => 'textfield',
				       '#title' => t('The link the image points to.'),
				       '#default_value' => $node->image_link,
				       );
  $form['image']['image_url'] = array(
				      '#type' => 'textfield',
				      '#title' => t('URL of the image.'),
				      '#default_value' => $node->image_url,
				      );
  return $form;
}

/**
 * Implementation of hook_load()
 */
function fooaggregator_feed_load(&$node) {
  return db_fetch_object(db_query("select * from {fooaggregator_feed} where nid=%d", $node->nid));
}

/**
 * Implementation of hook_insert()
 */
function fooaggregator_feed_insert(&$node) {
  db_query("insert into {fooaggregator_feed} (nid, url, visible, refresh, image_title, image_url, image_link) VALUES(%d, '%s', %d, %d, '%s', '%s', '%s')", $node->nid, $node->url, $node->visible, $node->refresh, $node->image_title, $node->image_url, $node->image_link);
}

/**
 * Implementation of hook_update()
 *
 * Update only the parts that are editable by the user.
 */
function fooaggregator_feed_update(&$node) {
  db_query("update {fooaggregator_feed} set url='%s', visible=%d, refresh=%d, image_title='%s', image_url='%s', image_link='%s' where nid=%s", $node->url, $node->visible, $node->refresh, $node->image_title, $node->image_url, $node->image_link, $node->nid);
}

/**
 * Update the non-editable parts.
 */
function _fooaggregator_feed_update(&$node) {

  // todo: discuss with alaa.
  db_query("update {fooaggregator_feed} set link = '%s', etag = '%s', checked = %d, updated = %d, modified = %d, image_title = '%s', image_link = '%s', image_url = '%s'  where nid=%d", $node->link, $node->etag, $node->checked, $node->updated, $node->modified, $node->image_title, $node->image_link, $node->image_url, $node->nid);
  //  db_query("update {fooaggregator_feed} set link = '%s', etag = '%s', checked = %d, updated = %d, modified = %d where nid=%d", $node->link, $node->etag, $node->checked, $node->updated, $node->modified, $node->nid);
}

/**
 * Implementation of hook_delete()
 */
function fooaggregator_feed_delete(&$node) {
  // todo: what if the cron is running and it updated the feed after we deleted it ?
  db_query("delete from {fooaggregator_feed} where nid=%d", $node->nid);
}

/**
 * Implementation of hook_view()
 */
function fooaggregator_feed_view(&$node, $teaser = FALSE, $page = FALSE) {
  $node = node_prepare($node, $teaser);
  $node->content['fooaggregator_feed'] = array(
					       '#value' => theme('fooaggregator_feed', $node),
					       '#weight' => -1,
					       );
  return $node;
}

/**
 * theme the feed information part.
 *
 * @param $feed a fooaggregator_feed node
 * @return an HTML string representing the themed feed information.
 */
function theme_fooaggregator_feed(&$feed) {
  $output .= theme('fooaggregator_feed_image', $feed);
  $output .= $feed->link ? l($feed->title, $feed->link).'<br />' : '';
  $output .= t('Checked: %time ago<br />', array('%time' => format_interval(time()-$feed->checked)));
  $output .= t('Updated: %time ago<br />', array('%time' => format_interval(time()-$feed->updated)));
  $output .= t('Update every: %time<br />', array('%time' => format_interval($feed->refresh)));
  $output .= '<br />'.filter_xss($feed->description).'<br />';

  $output .= theme('feed_icon', $feed->url);
  return theme('box', '', $output);
}

/**
 * theme the feed image.
 *
 * @param $feed a fooaggregator_feed node
 * @return an HTML string representing the themed feed image.
 */
function theme_fooaggregator_feed_image($feed) {
  if (($feed->image_link != '') && ($feed->image_url != '') && ($feed->image_title != '')) {
    $image = theme('image', url($feed->image_url), $feed->image_title, $feed->image_title, NULL, false);
    return '<div style="float: right;">'.l($image, url($feed->image_link), array('target' => 'new'), NULL, NULL, true, true).'</div>';
  }
}

/**
 * Implementation of hook_validate()
 */
function fooaggregator_feed_validate($node) {
  // flag an error if the URL is a duplicate.
  $result;
  if ($node->nid) {
    // editing.
    $result = db_query("select nid from {fooaggregator_feed} where url = '%s' and nid != %d", $node->url, $node->nid);
  }
  else {
    $result = db_query("select nid from {fooaggregator_feed} where url = '%s'", $node->url);
  }
  if (db_num_rows($result) > 0) {
    form_set_error('url', t('this URL has been used already'));
  }
}

/**
 * Clear a feed items
 *
 * @param $fid a feed ID
 */
function fooaggregator_feed_clear($fid) {
  $node = node_load($fid);
  $node->checked = 0;
  $node->updated = 0;
  $node->modified = 0;
  $node->etag = '';

  _fooaggregator_feed_update($node);
}

?>
