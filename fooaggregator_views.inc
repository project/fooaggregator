<?php // -*-php-*-

/**
 * Implementation of hook_views_style_plugins()
 */
function fooaggregator_views_style_plugins() {
  return array(
	       'fooaggregator' => array(
					'name' => t('Fooaggregator Admin'),
					'theme' => 'fooaggregator_views_view',
					'needs_fields' => true,
					'needs_table_header' => true,
					'validate' => 'fooaggregator_views_validate',
					'weight' => 100,
					),
	       );
}

/**
 * Implementation of hook_views_tables()
 */
function fooaggregator_views_tables() {
  $tables['fooaggregator_feed'] = _fooaggregator_feed_views_tables();
  $tables['fooaggregator_item'] = _fooaggregator_item_views_tables();
  $tables['fooaggregator_count'] = _fooaggregator_count_views_tables();
  $tables['fooaggregator_latest'] = _fooaggregator_latest_views_tables();
					 
  return $tables;
}


/**
 * Implementation of hook_views_arguments()
 */
function fooaggregator_views_arguments() {
  $ret = array(
	       'fid' => array(
			      'name' => t('Feed: Feed ID'),
			      'handler' => 'fooaggregator_views_handler_argument_fid',
			      'option' => array(
						'#type' => 'select',
						'#options' => array(
								    'equal',
								    'not equal',
								    ),
						),
			      ),
	       );

  return $ret;
}

/**
 * Implementation of hook_views_default_views()
 */
function fooaggregator_views_default_views() {
  // Admin view
  $view = new stdClass();
  $view->name = 'fooaggregator_admin';
  $view->description = t('This is the fooaggregator administration page.');
  $view->access = array();
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'News aggregator';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'fooaggregator';
  $view->url = 'admin/content/fooaggregator';
  $view->use_pager = FALSE;
  $view->nodes_per_page = '0';
  $view->menu = TRUE;
  $view->menu_title = 'News Aggregator';
  $view->menu_tab = FALSE;
  $view->menu_tab_weight = '0';
  $view->menu_tab_default = FALSE;
  $view->menu_tab_default_parent = NULL;
  $view->menu_parent_tab_weight = '0';
  $view->menu_parent_title = '';
  $view->sort = array();
  $view->argument = array();
  $view->field = array(
		       array(
			     'tablename' => 'node',
			     'field' => 'title',
			     'label' => 'Title',
			     'handler' => 'views_handler_field_nodelink',
			     'options' => 'link',
			     'sortable' => true,
			     ),
		       array(
			     'tablename' => 'fooaggregator_count',
			     'field' => 'items',
			     'label' => 'Items',
			     'sortable' => true,
			     ),
		       array(
			     'tablename' => 'fooaggregator_feed',
			     'field' => 'checked',
			     'label' => 'Last check',
			     'handler' => 'views_handler_field_date_small',
			     'sortable' => true,
			     ),
		       array(
			     'tablename' => 'fooaggregator_feed',
			     'field' => 'updated',
			     'label' => 'Last update',
			     'handler' => 'views_handler_field_date_small',
			     'sortable' => true,
			     ),
		       array(
			     'tablename' => 'fooaggregator_feed',
			     'field' => 'next_update',
			     'label' => 'Next update',
			     //'sortable' => true,
			     ),
		       array(
			     'tablename' => 'node',
			     'field' => 'edit',
			     'label' => '',
			     'handler' => 'views_handler_node_edit_destination',
			     ),
		       array(
			     'tablename' => 'node',
			     'field' => 'delete',
			     'label' => '',
			     'handler' => 'views_handler_node_delete_destination',
			     ),
		       array(
			     'tablename' => 'fooaggregator_feed',
			     'field' => 'update',
			     'label' => '',
			     ),
		       array(
			     'tablename' => 'fooaggregator_feed',
			     'field' => 'clear',
			     'label' => '',
			     ),
		       );
  $view->filter = array(
			array (
			       'tablename' => 'node',
			       'field' => 'type',
			       'operator' => 'OR',
			       'value' => array ('fooaggregator_feed'),
			       ),
			);
  $view->exposed_filter = array();
  $view->requires = array('node', 'fooaggregator_feed');
  $views[$view->name] = $view;

  return $views;
}

function fooaggregator_views_handler_field_image($fieldinfo, $fielddata, $value, $data) {
  $image = new stdClass;
  $image->image_link = $data->fooaggregator_feed_image_link;
  $image->image_title = $data->fooaggregator_feed_image_title;
  $image->image_url = $data->fooaggregator_feed_image_url;

  return theme('fooaggregator_feed_image', $image);
}

function fooaggregator_views_handler_field_link($fieldinfo, $fielddata, $value, $data) {
  return $value ? l($data->node_title, $value) : 'n/a';
}

function fooaggregator_views_handler_field_refresh($fieldinfo, $fielddata, $value, $data) {
  return format_interval($value);
}

function fooaggregator_views_handler_field_next_update($fieldinfo, $fielddata, $value, $data) {
  return $data->fooaggregator_feed_checked ? t('%time left', array('%time' => format_interval($data->fooaggregator_feed_checked + $data->fooaggregator_feed_refresh - time()))) : t('never');
}

function fooaggregator_views_handler_field_visible($fieldinfo, $fielddata, $value, $data) {
  return $value == 1 ? t('Yes') : t('No');
}

function fooaggregator_views_query_handler_field_link($fielddata, $fieldinfo, &$query) {
  $query->add_field('title', 'node');
}

function fooaggregator_views_handler_field_update($fieldinfo, $fielddata, $value, $data) {
  if (user_access('administer aggregator')) {
    return l(t('Refresh'), 'node/'.$data->nid.'/refresh', NULL, drupal_get_destination());
  }
}

function fooaggregator_views_handler_field_clear($fieldinfo, $fielddata, $value, $data) {
  if (user_access('administer aggregator')) {
    return l(t('Clear'), 'node/'.$data->nid.'/clear', NULL, drupal_get_destination());
  }
}

function fooaggregator_views_query_handler_field_items($fielddata, $fieldinfo, &$query) {
  $query->add_table('fooaggregator_count', true);
  $query->add_field('count(fooaggregator_count.nid)', '', 'fooaggregator_count_items');
  $query->add_groupby('fooaggregator_feed.nid');
}

function fooaggregator_views_query_handler_field_latest($fielddata, $fieldinfo, &$query) {
  $query->add_table('fooaggregator_latest', true);
  $query->add_field('max(fooaggregator_latest.created)', '', 'fooaggregator_latest_latest');
  $query->add_groupby('fooaggregator_count.fid');
}

function _fooaggregator_feed_views_tables() {
  $ret = array(
	       'name' => 'fooaggregator_feed',
	       'join' => array(
			       'type' => 'left',
			       'left' => array(
					       'table' => 'node',
					       'field' => 'nid',
					       ),
			       'right' => array(
						'field' => 'nid',
						),
			       ), // end join.
	       'fields' => array(
				 'link' => array(
						 'name' => t('Feed: Link'),
						 'handler' => 'fooaggregator_views_handler_field_link',
						 'query_handler' => 'fooaggregator_views_query_handler_field_link',
						 ),
				 'refresh' => array(
						    'name' => t('Feed: Update interval'),
						    'handler' => 'fooaggregator_views_handler_field_refresh',
						    'sortable' => true,
						    ),
				 'checked' => array(
						    'name' => t('Feed: Last checked'),
						    'handler' => views_handler_field_dates(),
						    'sortable' => true,
						    'option' => 'string',
						    ),
				 'updated' => array(
						    'name' => t('Feed: Last updated'),
						    'handler' => views_handler_field_dates(),
						    'sortable' => true,
						    'option' => 'string',
						    ),
				 'modified' => array(
						     'name' => t('Feed: Last modified'),
						     'handler' => views_handler_field_dates(),
						     'sortable' => true,
						     'option' => 'string',
						     ),
				 'next_update' => array(
							'name' => t('Feed: Next update'),
							'handler' => 'fooaggregator_views_handler_field_next_update',
							'notafield' => true,
							'addlfields' => array('checked', 'refresh'),
							),
				 'image' => array(
						  'name' => t('Feed: Image'),
						  'notafield' => true,
						  'addlfields' => array('image_url', 'image_title', 'image_link'),
						  'handler' => 'fooaggregator_views_handler_field_image',
						  ),
				 'visible' => array( // todo: needed ?
						    'name' => t('Feed: Visible'),
						    'handler' => 'fooaggregator_views_handler_field_visible',
						    ),
				 'update' => array(
						   'name' => 'Feed: Update items link.',
						   'notafield' => true,
						   'handler' => 'fooaggregator_views_handler_field_update',
						   ),
				 'clear' => array(
						  'name' => 'Feed: Clear items link.',
						  'notafield' => true,
						  'handler' => 'fooaggregator_views_handler_field_clear',
						  ),
				 ), // end fields.
	       'filters' => array(
				  'visible' => array(
						     'name' => t('Feed: Visible'),
						     'list' => 'views_handler_operator_yesno',
						     'list-type' => 'select',
						     'operator' => array('=' => t('Equals')),
						     ),
				  ),
	       );

  return $ret;
}

function _fooaggregator_count_views_tables() {
  $ret =  array(
		'name' => 'fooaggregator_item',
		'join' => array(
				'type' => 'left',
				'left' => array('table' => 'fooaggregator_feed', 'field' => 'nid'),
				'right' => array('field' => 'fid'),
				),
		'fields' => array(
				  'items' => array(
						   'name' => t('Feed: Number of items.'),
						   'handler' => 'views_handler_field_int',
						   'query_handler' => 'fooaggregator_views_query_handler_field_items',
						   'notafield' => true,
						   'sortable' => true,
						   'help' => t('This adds the number of items per feed.'),
						   ),
				  ),
		);
  return $ret;
}

function _fooaggregator_latest_views_tables() {
  $ret =  array(
		'name' => 'node',
		'join' => array(
				'type' => 'left',
				'left' => array('table' => 'fooaggregator_count', 'field' => 'nid'),
				'right' => array('field' => 'nid'),
				),
		'fields' => array(
				  'latest' => array(
						   'name' => t('Feed: Latest item'),
						   'handler' => views_handler_field_dates(),
						   'option' => 'string',
						   'query_handler' => 'fooaggregator_views_query_handler_field_latest',
						   'notafield' => true,
						   'sortable' => true,
						   'help' => t('This adds the date of latest feed item.'),
						   ),

				  ),
		);
  return $ret;
}

function _fooaggregator_item_views_tables() {
  $ret = array(
	       'name' => 'fooaggregator_item',
	       'join' => array(
			       'type' => 'left',
			       'left' => array(
					       'table' => 'node',
					       'field' => 'nid',
					       ),
			       'right' => array(
						'field' => 'nid',
						),
			       ),
	       'fields' => array(
				 'link' => array(
						 'name' => t('Feed Item: Link'),
						 'handler' => 'fooaggregator_views_handler_field_link',
						 'query_handler' => 'fooaggregator_views_query_handler_field_link',
						 ),
				 ),
	       );

  return $ret;
}

function fooaggregator_views_handler_argument_fid($op, &$query, $argtype, $arg = '') {
  switch ($op) {
  case 'summary':
    $join = array(
		  'type' => 'inner',
		  'left' => array(
				  'table' => 'node',
				  'field' => 'nid',
				  ),
		  'right' => array(
				   'field' => 'fid',
				   ),
		  );

    $query->add_field('title', 'node');
    $query->add_table('fooaggregator_item', false, 1, $join);
    $query->add_field('fid', 'fooaggregator_item');
    $fieldinfo['field'] = 'fooaggregator_item.fid';
    return $fieldinfo;

    case 'sort':
      // nothing.
      break;

  case 'filter':
    $query->ensure_table('fooaggregator_item', true);
    $args = _views_break_phrase($arg);
    if ($args[0] == 'and') {
      $operator = $argtype['options'] ? '!=' : '=';
      foreach ($args[1] as $arg) {
	$query->add_where("fooaggregator_item.fid $operator %d", $arg);
      }
    }
    else {
      $query->add_where("fooaggregator_item.fid IN (%s)", implode(',', $args[1]));
    }
    break;

      case 'link':
	return l($query->title, $arg.'/'.$query->fid);
      case 'title':
	// TODO: Is this a hack ? ;-)
	return views_handler_arg_nid($op, $query, $argtype, $arg);
  }
}

/**
 * A views theme function.
 * Display the default admin view.
 */
function theme_fooaggregator_views_view($view, $nodes, $type) {
  $fields = _views_get_fields();

  $total = count($nodes);
  $needs_updating = 0;

  foreach ($nodes as $node) {
    $row = array();
    foreach ($view->field as $field) {
      if ($fields[$field['id']]['visible'] !== FALSE) {
	$cell['data'] = views_theme_field('views_handle_field', $field['queryname'], $fields, $field, $node, $view);
	$cell['class'] = "view-field ". views_css_safe('view-field-'. $field['queryname']);
	$row[] = $cell;
      }
    }

    if (time() - $node->fooaggregator_feed_checked > $node->fooaggregator_feed_refresh) {
      $rows[] = array('class' => 'error', 'data' => $row);
      ++$needs_updating;
    }
    else {
      $rows[] = $row;
    }
  }

  drupal_set_message(t('Total: %total. Needs updating: %needs_updating', array('%total' => $total, '%needs_updating' => $needs_updating)));

  return theme('table', $view->table_header, $rows);
}

/**
 * Validate our view.
 */
function fooaggregator_views_validate($type, $view, $form) {
  // todo: clean this a bit. It seems a bit hackish to me.
  $valid = false;

  foreach ($view['field'] as $field) {
    if (is_array($field)) {
      if ($field['id'] == 'fooaggregator_feed.next_update' || $field['id'] == 'fooaggregator_feed.refresh') {
	$valid = true;
	break;
      }
    }
  }
  
  if (!$valid) {
    form_error($form["$type-info"][$type . '_type'], t('Please add a Last checked or Next update field.'));
  }

  // We are still a table. We will ask views to validate for us too.
  views_ui_plugin_validate_table($type, $view, $form);
}

?>
