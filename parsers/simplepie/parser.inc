<?php // -*-php-*-


/**
 * Refresh a feed.
 *
 * @param $feed a fooaggreator_feed node to refresh.
 * @return false in case of an error otherwise true (Even if the feed doesn't have new content).
 */
function _fooaggregator_simplepie_refresh(&$feed) {
  $xml = NULL;
  if (!_fooaggregator_download($feed, $xml)) {
    drupal_set_message("failed to download feed $feed->title", 'error');
    return false;
  }
  else if ($xml == NULL) {
    // No error but we have no new items.
    _fooaggregator_feed_update($feed);    
    return true;    
  }
  
  //parse (inlined here to reduce memory)  
  require_once 'simplepie.inc';
  $simplepie =& new SimplePie();  
  $simplepie->set_stupidly_fast(true);
  $simplepie->set_raw_data($xml);
  $simplepie->init();
  $simplepie->set_feed_url($feed->url);
  $xml = NULL;

  $error = $simplepie->error();
  if ($error) {
    $message = t('Failed to parse %title, %error', array(
							 '%title' => $feed->title,
							 '%error' => $error,
							 ));
    _fooaggregator_log_error($message);    
    return false;
  }
      
  _fooaggregator_simplepie_update_feed_info($feed, $simplepie);

  _fooaggregator_simplepie_update_nodes($feed, $simplepie);

  _fooaggregator_feed_update($feed);  
  
  $simplepie = NULL;
  return true;
}

function _fooaggregator_simplepie_update_feed_info(&$feed, &$simplepie) {
  $modified = false;
  
  if ($simplepie->get_description() && $feed->body != $simplepie->get_description()) {
    $feed->body = $simplepie->get_description();
    $modified = true;
  }
  if ($simplepie->get_image_url() && $feed->image_url != $simplepie->get_image_url()) {
    $feed->image_url = $simplepie->get_image_url() ;
    $modified = true;
  }
  if ($simplepie->get_image_title() && $feed->image_title != $simplepie->get_image_title()) {
    $feed->image_title = $simplepie->get_image_title();
     $modified = true;
  }
  if ($simplepie->get_image_link() && $feed->image_link != $simplepie->get_image_link()) {
    $feed->image_link = $simplepie->get_image_link();
    $modified = true;
  }   
  if ($simplepie->get_permalink() && $feed->link != $simplepie->get_permalink()) {
    $feed->link = $simplepie->get_permalink();
    $modified = true;
  }
  
  //todo: modify title?
  if ($modified) {    
    $uid = $feed->uid;
    $created = $feed->created;
    $feed = node_submit($feed);
    $feed->uid = $uid;
    $feed->created = $created;
    node_save($feed);
  }
}


/**
 * Check the feed and see if we have new or updated items.
 * Create new nodes or update old ones if needed.
 * It will also log the number of new, updated and non-changed nodes.
 *
 * @param $feed a FooFeed object.
 */
function _fooaggregator_simplepie_update_nodes(&$feed, &$simplepie) {
  $a = 0; // added
  $u = 0; // updated
  $n = 0; // not changed.

  foreach ($simplepie->get_items() as $item) {
    // todo: better way not to overload the database ?
    $nid = _fooaggregator_guid_exists($item->get_id(), $feed->nid);
    if (!$nid) {
      _fooaggregator_simplepie_create_node($feed, $item);
      ++$a;
    }
    else {
      if (variable_get('fooaggregator_update_items', 1) && _fooaggregator_simplepie_update_node($item, $nid)) {
	++$u;
      }
      else {
	++$n;
      }
    }    
    $item->feed = NULL;
    $item->data = NULL;
  }
  _fooaggregator_log_notice(t('Updated feed %url. %a added. %u updated. %n not changed.', array('%url' => $feed->url, '%a' => $a, '%u' => $u, '%n' => $n)));
}

/**
 * Update a node if needed.
 *
 * @param $item a FooFeed item array
 * @param $nid the node ID to update its node.
 * @return true if we updated it otherwise false.
 */
function _fooaggregator_simplepie_update_node(&$item, $nid) {
  // update only if changed.
  $current_node = node_load($nid);
  if (($current_node->body != $item->get_content()) || ($current_node->link != $item->get_permalink())) {
    $node = _fooaggregator_simplepie_parse_item($item);
    $node->nid = $nid;
    _fooaggregator_item_save($node);
    return true;
  }  
  return false;
}

/**
 * Create a new node for a feed item.
 *
 * @param $item the FooFeed item
 * @param $fid the ID of the feed.
 */
function _fooaggregator_simplepie_create_node(&$feed, &$item) {
  $node = _fooaggregator_simplepie_parse_item($item);
  if (!$node['title'] ) { $node['title'] = $feed->title." : ".format_date($node['created'], 'small'); }
  
  $node += _fooaggregator_item_skel($feed->nid);
  _fooaggregator_item_save($node);
  return true;
}

function _fooaggregator_simplepie_parse_item(&$item) {
  $node = array();
  
  $node['title'] = $item->get_title();    
  $node['link'] = $item->get_permalink();
  $node['tags'] = array();
  $node['body'] = $item->get_content();
  $node['guid'] = $item->get_id();
  $node['created'] = $item->get_date();
  $node['time'] = $item->get_date('U');

  foreach ($item->get_categories() as $tag) {
    $node['tags'][] = $tag->get_term();
    $tag = NULL;
  }

  $enclosure = $item->get_enclosure();
  if ($enclosure) {$node['enclosures'] = $enclosure->get_link();}

  $author = $item->get_author();
  if ($author) {$node['author'] = $author->get_name();}
  
  $node['geo_lat'] = $item->get_latitude();
  $node['geo_long'] = $item->get_longitude();
  
  //fill missing values
  if (!$node['title']) {
    $node['title'] = truncate_utf8(trim(strip_tags($node['body'])), 80);
  }
  
  return $node;
}
