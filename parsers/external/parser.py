#!/usr/bin/python

# todo:
# user agent ;-)
# we are getting document empty although the file doesn't exist.
# Cleaning. This is my 1st real python program.
__author__ = "Mohammed Sameer <msameer@foolab.org>"
__version__ = "$Id$"

try:
    import sys
    import feedparser
    import tempfile
    import os
    import time
    import xmlrpclib
#    import pprint
except:
    print "ER: I wasn't able to load all the modules I need."
    sys.exit(1)

class FooException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class Parser:
    def __init__(self, args):
        # todo: bad
        url = args['url']
        etag = None
        modified = None
        if args.has_key('etag'):
            etag = args['etag']
        if args.has_key('modified'):
            modified = time.gmtime(int(args['modified']))

        self.feed = feedparser.parse(url, etag, modified)

        self.file = ""
        self.node = {}
        self.node['items'] = []
        try:
            error = self.feed["bozo_exception"]
            raise FooException(str(error))
        except FooException:
            raise
        except:
            pass

    def save(self):
        """Save the feed and feed items."""
        self.file = tempfile.mktemp()
#        try:
        self.save_http()
        self.save_feed()
        self.save_feed_items()
        res = xmlrpclib.dumps(tuple([self.node]), 'FooParser')
        open(self.file, 'w').write(res)
#        print res
#        pprint.pprint(self.node)
#        except:
#            os.unlink(self.file)
#            raise

    def get_file(self):
        """Get the file where the data was saved"""
        return self.file

    def save_http(self):
        """Save the HTTP information to our feed dictionary"""
# todo: http errors.
        try:
            self.node['etag'] = self.feed.etag
        except:
            pass
        try:
            self.node['modified'] = time.strftime('%s', self.feed.modified)
        except:
            pass
#        pprint.pprint(self.feed)
        self.node['http_code'] = self.feed.status
###

    def save_feed_item(self, item):
        """Save a feed item to a file"""
        _item = {}
        title = ""
        guid = ""
        link = ""
        content = ""
        tags = []
        tm = 0

        link = item.link

        try:
            title = item.title
            guid = item.id
            content = item.summary
        except:
            title = 'Untitled feed item.'
            pass

        try:
            guid = item.id
        except:
            guid = link
            pass

        try:
            content = item.summary
        except:
            content = item.content[0].value
            pass

        try:
            tm = item.published_parsed
        except:
            try:
                tm = item.updated_parsed
            except:
                try:
                    tm = item.created_parsed
                except:
                    tm = time.localtime()

        tm = time.strftime('%s', tm)

        try:
            _item['geo_long'] = item.geo_lon
        except:
            pass
        try:
            _item['geo_lat'] = item.geo_lat
        except:
            pass
        try:
            _item['geo_point'] = item.geo_point
        except:
            pass

        _item['title'] = title
        _item['link'] = link
        _item['body'] = content
        _item['guid'] = guid
        _item['time'] = tm

        try:
            _item['author'] = item.author
#            _item['author'] = item.author_detail.name
        except:
            pass

        try:
            tags.extend([tag.term for tag in item.tags])
            _item['tags'] = tags
        except:
            pass

        self.node['items'].append(_item)

    def save_feed_items(self):
        """Save the feed items to files"""
        for entry in self.feed.entries:
            self.save_feed_item(entry)

    def save_feed(self):
        """Saves the feed and feed image information to a feed file and image file"""
        try:
            self.node['feed_title'] = self.feed.feed.title
        except:
            self.node['feed_title'] = ''
            pass

        try:
            self.node['body'] = self.feed.feed.description
        except:
            pass

        try:
            self.node['link'] = self.feed.feed.link
        except:
            pass

        try:
            self.node['image_url'] = self.feed.feed.image.href
            self.node['image_link'] = self.feed.feed.image.link
            self.node['image_title'] = self.feed.feed.image.title
        except:
            pass

        self.node['url'] = self.feed.href

if __name__ == "__main__":
    args = {}
    if len(sys.argv) != 3 and len(sys.argv) != 5 and len(sys.argv) != 7:
        print "ER: usage: <-url URL> [-etag ETAG] [-modified MODIFIED]"
        sys.exit(1)
    else:
        x = 1
        while x < len(sys.argv):
            if sys.argv[x] == "-url":
                args['url'] = sys.argv[x+1]
            elif sys.argv[x] == "-etag":
                args['etag'] = sys.argv[x+1]
            elif sys.argv[x] == "-modified":
                args['modified'] = sys.argv[x+1]
            x = x+2
        try:
            res = Parser(args)
            res.save()
            print "OK: %s" % res.get_file()
            sys.exit(0)
        except FooException, e:
            print "ER: %s" % e.value,
            sys.exit(2)
