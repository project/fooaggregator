<?php // -*-php-*-

/**
 * Refresh a feed.
 *
 * @param $feed a fooaggreator_feed node to refresh.
 * @return false in case of an error otherwise true (Even if the feed doesn't have new content).
 */
function _fooaggregator_external_refresh(&$feed) {
  $old_feed = $feed;

  $args = array('-url '.$feed->url);

  if ($feed->etag) {
    $args[] = '-etag '.$feed->etag;
  }

  if ($feed->modified) {
    $args[] = '-modified '.$feed->modified;
  }

  // drupal_get_path('module', 'fooaggregator')."/parsers/extra//parser.py";
  $path = dirname(__FILE__)."/parser.py";
  $command = "python ".$path." ".implode($args, " ");
  $pipe = popen($command, 'r');

  if (!$pipe) {
    $message = t('Failed to parse %title, %error', array(
							 '%title' => $feed->title,
							 '%error' => "Failed to invoke the external process.",
							 ));
    drupal_set_message($message, 'error');
    _fooaggregator_log_error($message);
    return false;
  }

  $status = fgets($pipe);
  $code = substr($status, 0, 3);
  pclose($pipe);

  if ($code == "ER:") {
    $message = t('Failed to parse %title, %error. Command used: %command', array(
							 '%title' => $feed->title,
							 '%error' => "An error while downloading or parsing the feed: ".substr($status, 4),
							 '%command' => $command,
							 ));
    drupal_set_message($message, 'error');
    _fooaggregator_log_error($message);
    return false;
  }
  else if ($code == "OK:") {
    $file = substr($status, 4);
    return _fooaggregator_external_handle($file, $old_feed);
  }
  else {
    $message = t('Failed to parse %title, %error. Command used: %command', array(
							 '%title' => $feed->title,
							 '%error' => "An unknown error: ".$status,
							 '%command' => $command,
							 ));
    drupal_set_message($message, 'error');
    _fooaggregator_log_error($message);
    return false;
  }
}

/**
 * Update a node if needed.
 *
 * @param $item a FooFeed item array
 * @param $nid the node ID to update its node.
 * @return true if we updated it otherwise false.
 */
function _fooaggregator_external_update_node(&$item, $nid) {
  // update only if changed.
  $node = node_load($nid);
  if (($node->body != $item->body) || ($node->link != $item->link) || ($node->title != $item->title)) {
    $item->teaser = node_teaser($item->body, $item->format);
    $item->nid = $node->nid;

    _fooaggregator_item_save($item);
    return true;
  }
  return false;
}

/**
 * Create a new node for a feed item.
 *
 * @param $node an array that will be turned into a node.
 * @param $fid the ID of the feed.
 */
function _fooaggregator_external_create_node(&$node, $fid) {
  // todo: deal with empty title
  $node += _fooaggregator_item_create($fid);
  _fooaggregator_item_save($node);
}

function _fooaggregator_external_update_nodes(&$feed) {
  $a = 0; // added
  $u = 0; // updated
  $n = 0; // not changed.

  foreach ($feed->items as $item) {
    // todo: better way not to overload the database ?
    $nid = _fooaggregator_guid_exists($item['guid'], $feed->nid);
    if (!$nid) {
      _fooaggregator_external_create_node($item, $feed->nid);
      ++$a;
    }
    else {
      if (_fooaggregator_external_update_node($item, $nid)) {
	++$u;
      }
      else {
	++$n;
      }
    }
    $item = NULL;
  }
  _fooaggregator_log_notice(t('Updated feed %url. %a added. %u updated. %n not changed.', array('%url' => $feed->url, '%a' => $a, '%u' => $u, '%n' => $n)));
}

function _fooaggregator_external_handle(&$file, &$old_feed) {
  require_once("includes/xmlrpc.inc");

  $xml = file_get_contents(trim($file));
  if (!$xml) {
    $message = t('Failed to parse %title, %error', array(
							 '%title' => $feed->title,
							 '%error' => "Failed to read the temp file ".$file,
							 ));
    drupal_set_message($message, 'error');
    _fooaggregator_log_error($message);
    return false;
  }
  $xmlrpc_message = xmlrpc_message($xml);
  $xml = NULL;
  xmlrpc_message_parse($xmlrpc_message);
  $feed = (object)$xmlrpc_message->params[0];
  $xmlrpc_message = NULL;

  // any http error ?
  switch($feed->http_code) {
  case 304:
    _fooaggregator_log_notice(t('No new content for %url', array('%url' => $feed->url)));
    $feed->checked = time();
    break;
  case 200:
  case 302:
  case 307:
    $feed->checked = time();
    if ($feed->url != $old_feed->url) {
      _fooaggregator_log_warning(t('Updated URL for feed %title from %old to %new', array(
											  '%title' => $old_feed->url,
											  '%old' => $old_feed->url,
											  '%new' => $feed->url,
											  )));
    }
    if (count($feed->items) == 0) {
      break;
    }

    $feed->updated = $feed->checked;
    break;

  default:
    _fooaggregator_log_error(t('HTTP error %number while getting %url', array(
									      '%number' => $feed->http_code,
									      '%url' => $old_feed->url,
										)));
    //    $feed->checked = time();
    return false;
  }

  // Now let's save

  if (!$feed->updated) {
    $feed->updated = $old_feed->updated;
  }

  $feed->nid = $old_feed->nid;
  $feed->format = $old_feed->format;
  $feed->teaser = node_teaser($feed->body, $feed->format);

  if (($feed->body != $old_feed->body) || ($feed->link != $old_feed->link) || ($feed->image_url != $old_feed->image_url)) {
    $uid = $feed->uid;
    $feed = node_submit($feed);
    $feed->uid = $uid;
    node_save($feed);
  }

  _fooaggregator_external_update_nodes($feed);
  _fooaggregator_feed_update($feed);
  return true;
}

?>
