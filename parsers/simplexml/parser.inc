<?php // -*-php-*-

/**
 * Refresh a feed
 * @param $feed a fooaggregator_feed node to refresh
 * @return false in case of an error otherwse true
 */
function _fooaggregator_simplexml_refresh(&$feed) {
  $xml = NULL;
  if (!_fooaggregator_download($feed, $xml)) {
    drupal_set_message("failed to download feed $feed->title", 'error');
    return false;
  }
  elseif ($xml == NULL) {
    // No error but we have no new items
    _fooaggregator_feed_update($feed);
    return true;
  }
  
  //parse
  if (!defined('LIBXML_VERSION') || (version_compare(phpversion(), '5.1.0', '<'))) {
    @ $parsed_xml = simplexml_load_string($xml, NULL);
  }
  else {
    @ $parsed_xml = simplexml_load_string($xml, NULL, LIBXML_NOERROR | LIBXML_NOWARNING);
  }  
  $xml = NULL;

  // We got a malformed XML
  if ($parsed_xml === FALSE || $parsed_xml == NULL) {
    $message = t('Failed to parse %title, %error', array('%title' => $feed->title));
    _fooaggregator_log_error($message);        
    return FALSE;
  }
  
  _fooaggregator_simplexml_update_feed_info($feed, $parsed_xml);
  
  _fooaggregator_simplexml_update_nodes($feed, $parsed_xml);
  
  _fooaggregator_feed_update($feed);

  $parsed_xml = NULL;
  return true;
}

function _fooaggregator_simplexml_update_feed_info(&$feed, &$xml) {
  switch ($xml->getName()) {
    //rdf similar to rss in feed metadata
  case 'rdf':
  case 'rss':
    //title? pubdate?
    $description = (string) $xml->channel->description;
    $link = (string) $xml->channel->link;    
    $image_url = (string) $xml->channel->image->url;
    $image_title = (string) $xml->channel->image->title;
    $image_link = (string) $xml->channel->image->link;      
    break;
  case 'feed' /*atom*/:
    $description = (string) $xml->tagline ? (string) $xml->tagline : (string) $xml->subtitle;
    //fixme: check for rel => alternative
    $link = (string) $xml->link['href'];
    $image_url = (string) $xml->icon;
    
    break;
  }
  
  $modified = false;
  if ($description && $feed->body != $description) {
    $feed->body = $description;
    $modified = true;
  }
  if ($image_url && $feed->image_url != $image_url) {
    $feed->image_url = $image_url ;
    $modified = true;
  }
  if ($image_title && $feed->image_title != $image_title) {
    $feed->image_title = $image_title ;
    $modified = true;
  }
  if ($image_link && $feed->image_link != $image_link) {
    $feed->image_link = $image_link;
    $modified = true;
  }
  if ($link && $feed->link != $link) {
    $feed->link = $link;
    $modified = true;
  }

  if ($modified) {    
    $uid = $feed->uid;
    $created = $feed->created;
    $feed = node_submit($feed);
    $feed->uid = $uid;
    $feed->created = $created;
    node_save($feed);
  }  
}

function _fooaggregator_simplexml_update_nodes(&$feed, &$xml) {
  $a = 0; //added
  $u = 0; //updated
  $n = 0; //not changed
  
  switch ($xml->getName()) {
  case 'rss':
    $items = $xml->channel->item;
    break;
  case 'rdf':
    $items = $xml->item;
    break;
  case 'feed' /*atom*/:
    $items = $xml->entry;
    break;
  }
  
  foreach ($items as $item) {
    $node = _fooaggregator_simplexml_parse_item($item, $xml->getName());
    if ($node['guid']) { $nid = _fooaggregator_guid_exists($node['guid'], $feed->nid); }
    
    if (!$node['title'] ) { $node['title'] = $feed->title." : ".$node['date']; }
    
    if ($nid) {
      if (!variable_get('fooaggregator_update_items', 1)) {
	++$n;
	continue;
      }
      $current_node = node_load($nid);
      if ($current_node->body == $node['body'] && $current_node->link == $node['link']) {
	++$n;
	continue;
      }
      ++$u;
    } else {
      $node += _fooaggregator_item_skel($feed->nid);
      ++$a;
    }
	
    if ($nid) { $node->nid = $nid; }    
    _fooaggregator_item_save($node);
    $item = NULL;
  }
  $items = NULL;  
  _fooaggregator_log_notice(t('Updated feed %url. %a added. %u updated. %n not changed.', array('%url' => $feed->url, '%a' => $a, '%u' => $u, '%n' => $n)));
}

function _fooaggregator_simplexml_parse_item(&$item, $feedtype) {
  $node = array();
  
  $namespaces = $item->getNamespaces(true);
  $node['title'] = (string) $item->title;    
  $node['link'] = (string) $item->link;
  $node['tags'] = array();
  switch ($feedtype) {
  case 'rdf':
  case 'rss':      
    $node['body'] = (string) $item->description;
    $node['guid'] = (string) $item->guid;
    $node['date'] = (string) $item->pubDate;
    foreach ($item->category as $tag) {
      $node['tags'][] = (string) $tag;
    }
    $node['enclosures'] = (string) $item->enclosure['url'];
    break;
  case 'feed' /* atom */:
    $node['body'] = $item->content ? (string) $item->content : (string) $item->summary;
    $node['guid'] = (string) $item->id;
    $node['date'] = $item->published ? (string) $item->published :  $item->issued ? (string) $item->issued : $item->created ? (string) $item->created : $item->updated ? (string) $item->updated : (string) $item->modified;
    $node['author'] = (string) $item->author->name;    
    foreach ($item->category as $tag) {
      $node['tags'][] = (string) $tag['term'];
    }
    $node['enclosures'] = (string) $item->enclosure['url'];
    break;
  }
  
  //namespaces
  if ($namespaces['content']) {
    $children = $item->children('content', true);
    $node['body'] = $children->encoded ? (string) $children->encoded : $node['body'];
    $children = NULL;
  }
  if ($namespaces['dc']) {
    $children = $item->children('dc', true);
    if (!$node['author']) { $node['author'] = (string) $children->creator;}
    if (!$node['date']) { $node['date'] = (string) $children->date; }
    if (!$node['tags']) {
      foreach ($children->subject as $tag) {
	$node['tags'][] = (string) $tag;
      }
    }
    $children = NULL;
  }
  if ($namespaces['geo']) {
    $children = $item->children('geo', true);
    $node['geo_lat'] = (string) $children->lat;
    $node['geo_long'] = (string) $children->long;
    $children = NULL;
  }
    
  //fill missing values
  if (!$node['title']) {
    $node['title'] = truncate_utf8(trim(strip_tags($node['body'])), 80);
  }
  if (!$node['guid']) {
    $node['guid'] =  $node['link'] ?  $node['link'] : $node['title'];
  }
  
  //parse date
  $node['time'] = strtotime($node['date']);

  return $node;
}

