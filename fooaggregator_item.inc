<?php // -*-php-*-

/**
 * Implementation of hook_form()
 */
function fooaggregator_item_form(&$node, &$param) {
  $type = node_get_types('type', $node);

  $form['title'] = array(
			 '#type' => 'textfield',
			 '#title' => check_plain($type->title_label),
			 '#required' => TRUE,
			 '#default_value' => $node->title,
			 '#weight' => -5
			 );

  $form['body_filter']['body'] = array(
				       '#type' => 'textarea',
				       '#title' => check_plain($type->body_label),
				       '#default_value' => $node->body,
				       '#required' => FALSE
				       );

  // we will overwrite it anyway if we update the feed item.
  $form['body_filter']['filter'] = filter_form($node->format);

  // todo: link should be text here and in the db ?
  $form['link'] = array(
			'#type' => 'textfield',
			'#title' => t('Link to the item'),
			'#default_value' => $node->link,
			);

  $form['author'] = array(
			  '#type' => 'textfield',
			  '#title' => t('Author of the item'),
			  '#default_value' => $node->author,
			  );
  return $form;
}

/**
 * Implementation of hook_load()
 */
function fooaggregator_item_load(&$node) {
  return db_fetch_object(db_query("select * from {fooaggregator_item} where nid=%d", $node->nid));
}

/**
 * Implementation of hook_insert()
 */
function fooaggregator_item_insert(&$node) {
  db_query("insert into {fooaggregator_item} (nid, fid, guid, link, author) VALUES(%d, %d, '%s', '%s', '%s')", $node->nid, $node->fid, $node->guid, $node->link, $node->author);
}

/**
 * Implementation of hook_update()
 *
 * Update the editable parts.
 */
function fooaggregator_item_update(&$node) {
  db_query("update {fooaggregator_item} set link='%s', author='%s' where nid=%d", $node->link, $node->author, $node->nid);
}

/**
 * Update the non-editable parts.
 */
function _fooaggregator_item_update(&$node) {
  db_query("update {fooaggregator_item} set guid='%s' where nid=%d", $node->guid, $node->nid);
}

/**
 * Implementation of hook_delete()
 */
function fooaggregator_item_delete(&$node) {
  db_query("delete from {fooaggregator_item} where nid=%d", $node->nid);
}

/**
 * Implementation of hook_view()
 */
function fooaggregator_item_view(&$node, $teaser = FALSE, $page = FALSE) {
  $node = node_prepare($node, $teaser);
  $node->content['fooaggregator_author'] = array(
						 '#value' => theme('fooaggregator_item_author', $node),
						 '#weight' => -1,
						 );
  return $node;
}

/**
 * theme the author part of the feed item.
 *
 * @param $node a fooaggregator_item node object
 * @return an HTML string representing the themed feed item author.
 */
function theme_fooaggregator_item_author(&$node) {
  $node->feed = node_load($node->fid);
  return l($node->feed->title, 'node/'.$node->feed->nid).' - '.format_date($node->created).' By '.check_plain($node->author).'<br />';
}

/**
 * Create a basic fooaggregator_item node.
 *
 * @param $fid a feed id
 * @param $item a reference to an array or object to manipulate. It will be casted to an object.
 */
function _fooaggregator_item_skel($fid) {
  $node = array();
  $node['type'] = 'fooaggregator_item';
  $node_options = variable_get('node_options_fooaggregator_item', array('status', 'promote'));
  $node['status'] = in_array('status', $node_options);
  $node['promote'] = in_array('promote', $node_options);
  if (module_exists('comment')) {
    $node['comment'] = variable_get("comment_fooaggregator_item", COMMENT_NODE_DISABLED);
  }
  $node['format'] = variable_get('fooaggregator_filter', variable_get('filter_default_format', 1));
  $node['fid'] = $fid;
  return $node;
}

/**
 * Save a generated node
 * The parser must set $node->time to a unix timestamp that will be used as changed and/or created
 *
 * @param $node a reference to a node to save.
 */
function _fooaggregator_item_save(&$node) {
  $uid =  variable_get('fooaggregator_uid', 1);
  $node = node_submit($node);
  $node->uid = $uid;
  
  if (!$node->nid) {
    $node->created = $node->time;
  }

  node_save($node);
}
