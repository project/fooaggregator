Why ?
I had enough. A lot of RSS feed aggregators around, most of them become unmaintained after a while.
I don't want to depend on one of them just to end up maintaining a code base I don't want to maintain
or find myself a migration path.

While I can't promise the best code or performance, I can promise this code will be maintained until I find
a better alternative (better drupal core module). I'll then provide a migration path and that's it.

Another point is that we will try to minimize the code written by depending on other well maintained modules
like views.

